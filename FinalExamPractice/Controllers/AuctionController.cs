﻿using FinalExamPractice.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalExamPractice.Controllers
{
    public class AuctionController : Controller
    {
        private IItemManager _itemManager;

        public AuctionController(IItemManager itemManager)
        {
            _itemManager = itemManager;
        }
        // GET: Auction
        public ActionResult ItemsView()
        {
            return View(_itemManager.ItemList);
        }

        public ActionResult PlaceBid(string Bid, int itemId)
        {
            decimal bidParsed = decimal.Parse(Bid);
            User user = Session["User"] as User;
            _itemManager.UpdateItemBid(itemId, user.Username, bidParsed);
            return RedirectToAction("ItemsView");

        }
    }
}