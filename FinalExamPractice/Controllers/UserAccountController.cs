﻿using FinalExamPractice.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalExamPractice.Controllers
{
    public class UserAccountController : Controller
    {
        private IUserManager _userManager;

        public UserAccountController(IUserManager userManager)
        {
            // make a variable equal the IUserManager binding
            _userManager = userManager;
        }
        // GET: UserAccount
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult CreateAccount()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateAccount(User user)
        {
            _userManager.AddUser(user);
            return RedirectToAction("Index");
        }

       
       
        public ActionResult Login(string username,string password)
        {
            User verfiyingUser = _userManager.RetrieveUsers.FirstOrDefault(x=>x.Username == username && x.Password == password);
            if(verfiyingUser != null)
            {
                Session["User"] = verfiyingUser;
                return RedirectToAction("ItemsView", "Auction");
            }else
            {
                return View("Index");
            }
        }
    }
}