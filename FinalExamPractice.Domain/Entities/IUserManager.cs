﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExamPractice.Domain.Entities
{
    public interface IUserManager
    {
        /// <summary>
        /// Retrieve a;; the users
        /// </summary>
        /// <returns></returns>
        IEnumerable<User> RetrieveUsers { get; }
        void AddUser(User user);
      

    }
}
