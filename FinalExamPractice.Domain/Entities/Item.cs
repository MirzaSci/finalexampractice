﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExamPractice.Domain.Entities
{
   public class Item
    {
        public int ItemId { get; set; }
        public string Name { get; set; }
        public decimal CurrentBidPrice { get; set; }
        public string HolderName { get; set; }


        public void Change(string holdername, decimal newbid)
        {
           this.HolderName = holdername;
            this.CurrentBidPrice = newbid;
        }
    }
}
