﻿using FinalExamPractice.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExamPractice.Domain.Entities
{
    class ItemManager : IItemManager
    {
        private IItemRepository _itemRepo;
        public ItemManager(IItemRepository itemRepo)
        {
            _itemRepo = itemRepo;
        }
        public IEnumerable<Item> ItemList
        {
            get
            {
                return _itemRepo.ItemTable;
            }
        }

        public void UpdateItemBid(int itemId, string username, decimal bid)
        {
            Item itemEntity = _itemRepo.ItemTable.FirstOrDefault(x=>x.ItemId==itemId);
            if(bid > itemEntity.CurrentBidPrice)
            {
                _itemRepo.UpdateItemBidInfo(itemEntity, username, bid);
            }
           
        }
    }
}
