﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExamPractice.Domain.Entities
{
   public interface IItemManager
    {
        IEnumerable<Item> ItemList { get; }
        void UpdateItemBid(int itemId,string username,decimal bid);
    }
}
