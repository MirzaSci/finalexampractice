﻿using FinalExamPractice.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExamPractice.Domain.Entities
{
     class UserManager : IUserManager
    {
        private IUserRespository _userRepo;
        public UserManager(IUserRespository userRepo)
        {
            _userRepo = userRepo;
        }

        public IEnumerable<User> RetrieveUsers
        {
            get
            {
                return _userRepo.UserTable;
            }
        }

        public void AddUser(User user)
        {
            _userRepo.SaveUser(user);
        }

       
       
       
    }

    }

