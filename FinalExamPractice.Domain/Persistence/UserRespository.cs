﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalExamPractice.Domain.Entities;

namespace FinalExamPractice.Domain.Persistence
{
    class UserRespository : IUserRespository
    {
        private AuctionDbContext _dbContext;
        public UserRespository()
        {
            _dbContext = new AuctionDbContext();
        }
        public IEnumerable<User> UserTable
        {
            get
            {
                return _dbContext.Users;
            }

           
        }

        public void SaveUser(User user)
        {
            if(user.UserId == 0)
            {
                _dbContext.Users.Add(user);
                
            }
            else
            {
                //todo
            }
            _dbContext.SaveChanges();
        }
    }
}
