﻿using FinalExamPractice.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExamPractice.Domain.Persistence
{
   public interface IItemRepository
    {
        IEnumerable<Item> ItemTable { get; }

        void UpdateItemBidInfo(Item item,string username, decimal amount);
    }
}
