﻿using FinalExamPractice.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExamPractice.Domain.Persistence
{
    class AuctionDbContext:DbContext
    {
        public AuctionDbContext() : base("AuctionAppDbConnection")
        {

        }

        public DbSet<User> Users { get; set; }


        public DbSet<Item> Items { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<User>().ToTable("User");
        //}
    }
}
