﻿using FinalExamPractice.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExamPractice.Domain.Persistence
{
   public interface IUserRespository
    {
        IEnumerable<User> UserTable { get;}
        void SaveUser(User user);
    }
}
