﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalExamPractice.Domain.Entities;

namespace FinalExamPractice.Domain.Persistence
{
    class ItemRepoository : IItemRepository
    {
        private AuctionDbContext _dbcontext;
        public ItemRepoository()
        {
            _dbcontext = new AuctionDbContext();
        }
        public IEnumerable<Item> ItemTable
        {
            get
            {
                return _dbcontext.Items; 
            }
        }

      
        public void UpdateItemBidInfo(Item item, string username, decimal amount)
        {
            Item updatingItem = _dbcontext.Items.Find(item.ItemId);
            updatingItem.Change(username, amount);
            _dbcontext.SaveChanges();
        }
    }
}
