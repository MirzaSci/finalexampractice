﻿using FinalExamPractice.Domain.Entities;
using FinalExamPractice.Domain.Persistence;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExamPractice.Domain
{
    public class DomainFactoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserManager>().To<UserManager>().InSingletonScope();
            Bind<IUserRespository>().To<UserRespository>().InSingletonScope();
            Bind<IItemRepository>().To<ItemRepoository>().InSingletonScope();
            Bind<IItemManager>().To<ItemManager>().InSingletonScope();

        }
    }
}
